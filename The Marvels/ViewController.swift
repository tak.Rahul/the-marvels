//
//  ViewController.swift
//  The Marvels
//
//  Created by Tak Rahul on 08/12/20.
//  Copyright © 2020 SysMind. All rights reserved.
//

import UIKit

class LoadAnimationVC: UIViewController {

    @IBOutlet weak var gifImgView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        guard let confettiImageView = UIImageView.fromGif(frame: gifImgView.frame, resourceName: "Marvel") else { return }
        view.addSubview(confettiImageView)
        confettiImageView.startAnimating()
        
    }
    
    func setupRoot() {
    }


}

